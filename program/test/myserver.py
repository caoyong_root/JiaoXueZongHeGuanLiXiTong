# import socket
#
# host = "127.0.0.1"
# port = 9000
#
# # 创建socket对象
# server = socket.socket()
# # 绑定ip，端口
# server.bind((host, port))
# # 设置监听,最多可有5个客户端进行排队
# server.listen(5)
# print("listen")
# while True:
#     # 阻塞状态，被动等待客户端的连接
#     conn, addr = server.accept()
#     print("accept")
#     print(conn)
#     print(addr)
#     while True:
#         # 接收数据
#         accept_data = conn.recv(1024)
#         print(accept_data)
#         accept_data2 = str(accept_data, encoding="utf8")
#         print(accept_data2)
#         print("".join(("接收内容：", accept_data2, "客户端口：", str(addr[1]))))
#         # if byebye
#         if accept_data2 == "byebye":
#             break
#         # 发送数据
#         send_data = input("输入发送内容：")
#         conn.sendall(bytes(send_data, encoding="utf8"))
#         if send_data == "byebye":
#             break
#     conn.close()

import socketserver  # 导入socketserver模块


class MyServer(socketserver.BaseRequestHandler):  # 创建一个类，继承自socketserver模块下的BaseRequestHandler类
    def handle(self):  # 要想实现并发效果必须重写父类中的handler方法，在此方法中实现服务端的逻辑代码（不用再写连接准备，包括bind()、listen()、accept()方法）
        while 1:
            conn = self.request
            addr = self.client_address
            # 上面两行代码，等于 conn,addr = socket.accept()，只不过在socketserver模块中已经替我们包装好了，还替我们包装了包括bind()、listen()、accept()方法
            while 1:
                accept_data = str(conn.recv(1024), encoding="utf8")
                print(addr, accept_data)
                if accept_data == "byebye":
                    break
                send_data = bytes(input(">>>>>"), encoding="utf8")
                print(addr)
                conn.sendall(send_data)
            conn.close()


if __name__ == '__main__':
    sever = socketserver.ThreadingTCPServer(("127.0.0.1", 8888),
                                            MyServer)  # 传入 端口地址 和 我们新建的继承自socketserver模块下的BaseRequestHandler类  实例化对象

    sever.serve_forever()  # 通过调用对象的serve_forever()方法来激活服务端