# import  socket
#
# host = "127.0.0.1"
# port = 8888
#
# client = socket.socket()
# client.connect((host, port))
# while True:
#     send_data = input("输入发送内容：")
#     client.sendall(bytes(send_data, encoding="utf8"))
#     if send_data == "byebye":
#         break
#     accept_data = client.recv(1024)
#     accept_data2 = str(accept_data, encoding="utf8")
#     print("".join(("接收内容：", accept_data2)))
#     if accept_data2 == "byebye":
#         break
# client.close()

import socket

sk = socket.socket()
sk.connect(("127.0.0.1", 8888))  # 主动初始化与服务器端的连接
while True:
    send_data = input("输入发送内容:")
    sk.sendall(bytes(send_data, encoding="utf8"))
    if send_data == "byebye":
        break
    accept_data = str(sk.recv(1024), encoding="utf8")
    print("".join(("接收内容：", accept_data)))
sk.close()