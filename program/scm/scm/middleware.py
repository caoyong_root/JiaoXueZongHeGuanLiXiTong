import time
def simple_middleware(get_request):

    def middleware(request):
        print('run before')
        start = time.time()
        response = get_request(request)
        print('run after')
        print('cost', time.time()-start)
        # 来源IP，执行用户，当前请求URL，花费时间
        return response

    return middleware