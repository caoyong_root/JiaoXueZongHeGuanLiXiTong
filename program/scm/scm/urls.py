from django.conf.urls import url, include
from django.contrib import admin
from apps.crm import views
from apps.count.views import job_info


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^index/$', views.index, name='index'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^login/$', views.login, name='login'),
    url(r'^register/$', views.register, name='register'),
    url(r'^student/', include('apps.student.urls', namespace='student')),
    url(r'^teacher/', include('apps.teacher.urls', namespace='teacher')),
    url(r'^apis/', include('apps.apis.urls', namespace='apis')),
    url(r'^job_count/', views.job_count, name='job_count'),
    url(r'count/', include('apps.count.urls', namespace='count')),
    url(r'^test_session/', views.test_session, name='test_session'),
    url(r'^test_celery/', views.test_celery, name='test_celery'),
    url(r'test_transaction', views.test_transaction, name='test_transaction')
]
