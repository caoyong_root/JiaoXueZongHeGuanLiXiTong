from django.conf.urls import url, include
from apps.teacher import views

urlpatterns = [
    url(r'^course/$', views.course, name='course'),
]
