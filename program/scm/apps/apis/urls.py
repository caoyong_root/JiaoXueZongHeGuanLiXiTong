from django.conf.urls import url, include
from apps.apis.views import job_count, course_record
from apps.student import views

urlpatterns = [
    url(r'^job_count/$', job_count, name='job_count'),
    url(r'^course_record/(\d+)/$', course_record, name="course_record"),
    url(r'^homework/(\d+)/$', views.homework, name='homework')
]
