from django.shortcuts import render, HttpResponse
import json
from apps.crm.models import CourseRecord, JobInfo
from apps.crm import forms
from django.contrib.auth.decorators import login_required
from apps.crm.models import User
# Create your views here.


def job_count(request):
    # b = [{"name": 'day', "data": [i for i in range(17)]}, {"name": "PV", "data": [5, 4, 3, 2, 8, 1, 2, 8, 1, 8, 3, 17, 9, 0, 3, 6, 13]},
    #      {"name": "UV", "data": [3, 7, 4, 1, 6, 1, 6, 2, 3, 9, 2, 5, 1, 13, 9, 6, 14]}]
    data = [{
        'type': 'pie',
        'name': '薪水',
            'data': [
                ['<3000', JobInfo.objects.filter(salary__lte=3000).count()],
                ['3000-5000', JobInfo.objects.filter(salary__gte=3000, salary__lte=5000).count()],
                ['5000-8000', JobInfo.objects.filter(salary__gt=5000, salary__lte=8000).count()],
                ['>8000', JobInfo.objects.filter(salary__gte=8000).count()],
            ]
        }]
    return HttpResponse(json.dumps(data))

@login_required
def course_record(request, id):
    form = forms.ScoreRecord()
    course_record = CourseRecord.objects.get(id=id)
    kwgs = {
            'course_record': course_record,
            'form': form
    }
    return render(request, 'student/course_record.html', kwgs)

