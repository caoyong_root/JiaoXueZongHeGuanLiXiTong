from django.conf.urls import url, include
from apps.student import views

urlpatterns = [
    url(r'^course/$', views.course, name='course'),
]
