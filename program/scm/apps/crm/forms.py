from django import forms

class LoginForm(forms.Form):
    email = forms.CharField(label='Email',
                             error_messages={'required': '请输入邮箱'},
                             widget=forms.EmailInput(
                                 attrs={
                                     'placeholder': 'Email',
                                     "class": "form-control"
                                 }
                             ))

    password = forms.CharField(label='密码',
                             error_messages={'required': '请输入密码'},
                             widget=forms.PasswordInput(
                                 attrs={
                                     'placeholder': '密码',
                                     "class": "form-control"
                                 }
                             ))

class RegisterForm(forms.Form):
    email  = forms.CharField(label='Email',
                             error_messages={'required': '请输入邮箱'},
                             widget=forms.EmailInput(
                                 attrs={
                                     'placeholder': 'Email',
                                     "class": "form-control"
                                 }
                             ))

    username = forms.CharField(label='用户名',
                               error_messages={'required': '请输入用户名'},
                               widget=forms.TextInput(
                                   attrs={
                                       'placeholder': '用户名',
                                       'class': "form-control"
                                   }
                               ))

    password = forms.CharField(label='密码',
                               error_messages={'required': '请输入密码'},
                               widget=forms.PasswordInput(
                                   attrs={
                                       'placeholder': '密码',
                                       "class": "form-control"
                                   }
                               ))

    password2 = forms.CharField(label='密码1',
                               error_messages={'required': '请再次输入密码'},
                               widget=forms.PasswordInput(
                                   attrs={
                                       'placeholder': '确认密码',
                                       "class": "form-control"
                                   }
                               ))


class ScoreRecord(forms.Form):
    answerdetails = forms.CharField(label='answerdetails',
                                    error_messages={'required': '请输入答案详情'},
                                    widget=forms.TextInput(
                                        attrs={
                                            'placeholder': '答题详情',
                                            'class': 'form-control'
                                        }
                                    ))
    enclosure = forms.CharField(label='作业附件',
                                # enctype = 'mulltipart/form-data',
                                widget=forms.FileInput(
                                    attrs={
                                        'placeholder': '作业附件',
                                        'class': 'form-control'
                                    }
                                ))

class HomeWorkForm(forms.Form):
    homework_replay_content = forms.CharField(label='homework_replay_content',
                                    error_messages={'required': '请输入答案详情'},
                                    widget=forms.TextInput(
                                        attrs={
                                            'placeholder': '答题详情',
                                            'class': 'form-control'
                                        }
                                    ))
    homework_replay_attach = forms.CharField(label='作业附件',
                                widget=forms.FileInput(
                                    attrs={
                                        'placeholder': '作业附件',
                                        'class': 'form-control'
                                    }
                                ))