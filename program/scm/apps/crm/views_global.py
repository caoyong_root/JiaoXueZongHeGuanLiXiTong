from apps.crm.models import Menu

def menu_info(request):
    if request.user.is_authenticated():
        roles = request.user.roles.all()
        q_menus = ""
        for role in roles:
            if q_menus:
                q_menus = q_menus | role.menus.all()
            else:
                q_menus = role.menus.all()
        if q_menus:
            q_menus = q_menus.distinct()
            menus = q_menus.filter(pid_menu=None)
            for menu in menus:
                menu.children = q_menus.filter(pid_menu=menu.id)
        print(locals())
        return locals()
    else:
        return locals()