from django.contrib import admin
from apps.crm import models
from apps.crm.models import  CourseRecord
# Register your models here.


class ClassListAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        obj.save()
        contents = obj.course.contents.all()
        for content in contents:
            models.CourseRecord.objects.create(class_name=obj,
                                               course_content=content,
                                               day_num=0)
        print('create course recoder')


admin.site.register(models.User)
admin.site.register(models.Student)
admin.site.register(models.Teacher)
admin.site.register(models.Role)
admin.site.register(models.Branch)
admin.site.register(models.CourseRecord)
admin.site.register(models.Course)
admin.site.register(models.ClassType)
admin.site.register(models.CourseContent)
admin.site.register(models.PreStudent)
admin.site.register(models.ClassList, ClassListAdmin)
admin.site.register(models.ScoreRecord)
admin.site.register(models.Menu)
admin.site.register(models.JobInfo)
admin.site.register(models.Preperty)
admin.site.register(models.CheckingInStatistics)
admin.site.register(models.CheckingIn)