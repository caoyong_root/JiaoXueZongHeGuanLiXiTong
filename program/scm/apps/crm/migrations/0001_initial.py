# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-21 07:14
from __future__ import unicode_literals

from django.conf import settings
import django.contrib.auth.models
import django.contrib.auth.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
                ('first_name', models.CharField(blank=True, max_length=30, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=30, verbose_name='last name')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('note', models.TextField(blank=True, default=None, null=True, verbose_name='备注')),
                ('date_joined', models.DateTimeField(auto_now_add=True, null=True)),
            ],
            options={
                'verbose_name': '系统用户',
                'verbose_name_plural': '系统用户',
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Branch',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, unique=True, verbose_name='校区名')),
            ],
            options={
                'verbose_name': '校区',
                'verbose_name_plural': '校区',
            },
        ),
        migrations.CreateModel(
            name='CheckingIn',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='考勤日期')),
                ('sign_time', models.TimeField(verbose_name='签到时间')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='用户')),
            ],
            options={
                'verbose_name': '考勤记录',
                'verbose_name_plural': '考勤记录',
            },
        ),
        migrations.CreateModel(
            name='CheckingInStatistics',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='考勤日期')),
                ('sign_time', models.TimeField(null=True, verbose_name='签到时间')),
                ('leave_time', models.TimeField(null=True, verbose_name='签退时间')),
                ('status', models.CharField(max_length=30, verbose_name='状态')),
                ('note', models.CharField(max_length=100, null=True, verbose_name='备注')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='用户')),
            ],
            options={
                'verbose_name': '考勤统计',
                'verbose_name_plural': '考勤统计',
            },
        ),
        migrations.CreateModel(
            name='ClassList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=32, verbose_name='班级名')),
                ('start_date', models.DateField(verbose_name='开班日期')),
                ('end_date', models.DateField(blank=True, null=True, verbose_name='结业日期')),
                ('branch', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crm.Branch', verbose_name='校区')),
            ],
            options={
                'verbose_name': '班级列表',
                'verbose_name_plural': '班级列表',
            },
        ),
        migrations.CreateModel(
            name='ClassType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='')),
            ],
            options={
                'verbose_name': '开班类型',
                'verbose_name_plural': '开班类型',
            },
        ),
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, unique=True, verbose_name='课程名')),
                ('cover', models.ImageField(blank=True, default='covers/default.jpg', max_length=128, upload_to='covers/', verbose_name='课程封面')),
                ('desc', models.TextField(verbose_name='课程描述')),
                ('outline', models.TextField(verbose_name='课程大纲')),
                ('cycle', models.IntegerField(verbose_name='课程周期(月)')),
            ],
            options={
                'verbose_name': '课程',
                'verbose_name_plural': '课程',
            },
        ),
        migrations.CreateModel(
            name='CourseContent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content_type', models.CharField(choices=[('Linux', 'Linux'), ('Python', 'Python'), ('NetWork', '网络'), ('MySQL', 'MySQL')], max_length=32, verbose_name='类别')),
                ('name', models.CharField(max_length=256, verbose_name='课时标题')),
                ('desc', models.TextField(verbose_name='课时详情')),
                ('homework_title', models.CharField(max_length=256, verbose_name='作业标题')),
                ('homework_content', models.TextField(verbose_name='作业内容')),
            ],
            options={
                'verbose_name': '课时',
                'verbose_name_plural': '课时',
            },
        ),
        migrations.CreateModel(
            name='CourseRecord',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('day_num', models.IntegerField(verbose_name='第N次课')),
                ('date', models.DateField(auto_now_add=True, verbose_name='上课日期')),
                ('homework_title', models.CharField(blank=True, max_length=256, null=True)),
                ('homework_desc', models.TextField(blank=True, null=True)),
                ('class_name', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crm.ClassList', verbose_name='班级(课程)')),
                ('course_content', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crm.CourseContent', verbose_name='上课内容')),
            ],
            options={
                'verbose_name': '上课记录',
                'verbose_name_plural': '上课记录',
            },
        ),
        migrations.CreateModel(
            name='JobInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('job_photo', models.ImageField(blank=True, default='job_photo/default.jpg', null=True, upload_to='job_photo/', verbose_name='照片')),
                ('company', models.CharField(blank=True, max_length=255, null=True, verbose_name='公司')),
                ('salary', models.IntegerField(blank=True, null=True, verbose_name='薪水')),
                ('city', models.CharField(blank=True, max_length=20, null=True, verbose_name='城市')),
                ('position', models.CharField(blank=True, max_length=64, null=True, verbose_name='岗位')),
                ('is_recommend', models.BooleanField(default=False, verbose_name='是否推荐')),
            ],
            options={
                'verbose_name': '就业信息',
                'verbose_name_plural': '就业信息',
            },
        ),
        migrations.CreateModel(
            name='Menu',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='菜单名')),
                ('url_name', models.CharField(max_length=128, unique=True, verbose_name='URL链接')),
                ('order', models.SmallIntegerField(default=1, verbose_name='排序')),
                ('pid_menu', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='crm.Menu', verbose_name='上级菜单')),
            ],
            options={
                'verbose_name': '导航信息',
                'verbose_name_plural': '导航信息',
            },
        ),
        migrations.CreateModel(
            name='Preperty',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(choices=[('display', '显示器'), ('keyboard', '键盘'), ('mouse', '鼠标'), ('mainframe', '主机'), ('table&chairs', '桌椅')], max_length=30, verbose_name='资产类型')),
                ('buy_time', models.DateField(verbose_name='购买时间')),
                ('amount', models.IntegerField(verbose_name='单价金额')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='用户')),
            ],
            options={
                'verbose_name': '资产表',
                'verbose_name_plural': '资产表',
            },
        ),
        migrations.CreateModel(
            name='PreStudent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('qq', models.CharField(max_length=64, unique=True, verbose_name='QQ号')),
                ('nick_name', models.CharField(blank=True, max_length=64, null=True, verbose_name='昵称')),
                ('real_name', models.CharField(blank=True, max_length=32, null=True, verbose_name='姓名')),
                ('sex', models.CharField(choices=[('male', '男'), ('female', '女')], default='male', max_length=32, verbose_name='性别')),
                ('birthday', models.DateField(blank=True, null=True, verbose_name='出生日期')),
                ('telephone', models.CharField(blank=True, max_length=11, null=True, verbose_name='手机号')),
                ('email', models.EmailField(blank=True, max_length=254, null=True, verbose_name='邮箱地址')),
                ('source', models.CharField(choices=[('qq', 'QQ群'), ('referral', '口碑推荐'), ('website', '官方网站'), ('school', '学校课程'), ('others', '其它')], default='other', max_length=64, verbose_name='学生来源')),
                ('st_note', models.TextField(verbose_name='具体内容')),
                ('work_status', models.CharField(choices=[('student', '学生'), ('employed', '在职'), ('unemployed', '无业')], default='student', max_length=32, verbose_name='职业状态')),
                ('company', models.CharField(blank=True, max_length=64, null=True, verbose_name='当前公司')),
                ('salary', models.CharField(blank=True, max_length=64, null=True, verbose_name='当前薪资')),
                ('status', models.CharField(choices=[('signed', '已报名'), ('unregistered', '未报名')], default='unregistered', max_length=64, verbose_name='是否报名')),
                ('date', models.DateField(auto_now_add=True, verbose_name='咨询日期')),
                ('class_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crm.ClassType', verbose_name='开班方式')),
                ('course', models.ManyToManyField(to='crm.Course', verbose_name='咨询课程')),
                ('referral_from', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='推荐人', to='crm.PreStudent')),
            ],
            options={
                'verbose_name': '意向学员表',
                'verbose_name_plural': '意向学员表',
            },
        ),
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=32, unique=True, verbose_name='角色名称')),
                ('menus', models.ManyToManyField(blank=True, to='crm.Menu', verbose_name='权限信息')),
            ],
            options={
                'verbose_name': '角色',
                'verbose_name_plural': '角色',
            },
        ),
        migrations.CreateModel(
            name='ScoreRecord',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('record', models.CharField(choices=[('checked', '正常'), ('late', '迟到'), ('leaved', '缺勤'), ('leave_early', '早退')], default='checked', max_length=64, verbose_name='上课纪录')),
                ('homework_replay_content', models.TextField(verbose_name='答题详情')),
                ('homework_replay_attach', models.FileField(default=True, null=True, upload_to='housework/', verbose_name='作业附件')),
                ('score', models.IntegerField(blank=True, choices=[(100, 'A+'), (90, 'A'), (85, 'B+'), (80, 'B'), (70, 'B-'), (60, 'C+'), (50, 'C'), (40, 'C-'), (-50, 'D'), (0, 'N/A')], null=True, verbose_name='本次成绩')),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('note', models.CharField(blank=True, max_length=255, null=True, verbose_name='备注')),
                ('course_record', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crm.CourseRecord', verbose_name='课程')),
            ],
            options={
                'verbose_name': '上课成绩纪录',
                'verbose_name_plural': '上课成绩纪录',
            },
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('avator', models.ImageField(blank=True, default='avator/default.jpg', null=True, upload_to='avator/teacher/', verbose_name='头像')),
                ('idcard', models.CharField(blank=True, max_length=18, null=True, verbose_name='身份证号')),
                ('date_end', models.DateField(verbose_name='失效日期')),
                ('detail', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crm.PreStudent', verbose_name='详细信息')),
                ('my_class', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crm.ClassList', verbose_name='所在班级')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='关联用户')),
            ],
            options={
                'verbose_name': '报名学员',
                'verbose_name_plural': '报名学员',
            },
        ),
        migrations.CreateModel(
            name='Teacher',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('avator', models.ImageField(blank=True, default='avator/default.jpg', null=True, upload_to='avator/teacher/', verbose_name='头像')),
                ('real_name', models.CharField(blank=True, max_length=32, null=True, verbose_name='姓名')),
                ('desc', models.CharField(max_length=512, verbose_name='描述信息')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='关联用户')),
            ],
            options={
                'verbose_name': '教师团队',
                'verbose_name_plural': '教师团队',
            },
        ),
        migrations.AddField(
            model_name='scorerecord',
            name='student',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crm.Student', verbose_name='学员'),
        ),
        migrations.AddField(
            model_name='jobinfo',
            name='student',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='crm.Student', verbose_name='学员ID'),
        ),
        migrations.AddField(
            model_name='courserecord',
            name='teacher',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crm.Teacher', verbose_name='讲师'),
        ),
        migrations.AddField(
            model_name='course',
            name='contents',
            field=models.ManyToManyField(to='crm.CourseContent', verbose_name='课程内容'),
        ),
        migrations.AddField(
            model_name='classlist',
            name='class_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crm.ClassType'),
        ),
        migrations.AddField(
            model_name='classlist',
            name='course',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crm.Course', verbose_name='课程'),
        ),
        migrations.AddField(
            model_name='classlist',
            name='teachers',
            field=models.ManyToManyField(to='crm.Teacher', verbose_name='讲师'),
        ),
        migrations.AddField(
            model_name='user',
            name='branch',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='crm.Branch', verbose_name='所属校区'),
        ),
        migrations.AddField(
            model_name='user',
            name='groups',
            field=models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups'),
        ),
        migrations.AddField(
            model_name='user',
            name='roles',
            field=models.ManyToManyField(blank=True, to='crm.Role', verbose_name='用户角色'),
        ),
        migrations.AddField(
            model_name='user',
            name='user_permissions',
            field=models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions'),
        ),
        migrations.AlterUniqueTogether(
            name='scorerecord',
            unique_together=set([('course_record', 'student')]),
        ),
        migrations.AlterUniqueTogether(
            name='courserecord',
            unique_together=set([('class_name', 'course_content')]),
        ),
        migrations.AlterUniqueTogether(
            name='classlist',
            unique_together=set([('course', 'name')]),
        ),
    ]
