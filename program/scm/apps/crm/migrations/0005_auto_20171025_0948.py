# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-25 01:48
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0004_auto_20171024_1622'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='my_class',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_course_record', to='crm.ClassList', verbose_name='所在班级'),
        ),
        migrations.AlterField(
            model_name='student',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_student', to=settings.AUTH_USER_MODEL, verbose_name='关联用户'),
        ),
    ]
