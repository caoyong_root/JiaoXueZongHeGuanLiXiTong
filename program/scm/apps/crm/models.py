from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.

class Menu(models.Model):
    '''侧边菜单'''
    # 如果表里面没有PK,Django会自动加一个名为id的主键（数字自增）
    name = models.CharField(max_length=64, verbose_name='菜单名')
    url_name = models.CharField(max_length=128, unique=True, verbose_name='URL链接')
    order = models.SmallIntegerField(default=1, verbose_name='排序')
    # 递归型的外键
    pid_menu = models.ForeignKey('self', blank=True, null=True, verbose_name='上级菜单')

    def __str__(self):
        return '{}<{}>'.format(self.name, self.url_name)

    class Meta:
        verbose_name = '导航信息'
        verbose_name_plural = verbose_name
        ordering = ['id']


class Branch(models.Model):
    '''分校区'''
    name = models.CharField(max_length=64, unique=True, verbose_name='校区名')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '校区'
        verbose_name_plural = verbose_name


class CourseContent(models.Model):
    '''课时详情-单次课'''
    content_type_choice = (('Linux', 'Linux'),
                           ('Python', 'Python'),
                           ('NetWork', '网络'),
                           ('MySQL', 'MySQL'),
                           )
    content_type = models.CharField(max_length=32, choices=content_type_choice, verbose_name='类别')
    name = models.CharField(max_length=256, verbose_name='课时标题')
    desc = models.TextField(verbose_name='课时详情')
    homework_title = models.CharField(max_length=256, verbose_name='作业标题')
    homework_content = models.TextField(verbose_name='作业内容')

    def __str__(self):
        return '<{}>{}'.format(self.content_type, self.name)

    class Meta:
        verbose_name = '课时详情'
        verbose_name_plural = verbose_name

class Course(models.Model):
    '''课程信息'''
    name = models.CharField(max_length=64, unique=True, verbose_name='课程名')
    cover = models.ImageField(upload_to='covers/', default='covers/default.jpg',
                              max_length=128, blank=True, verbose_name='课程封面')
    desc = models.TextField(verbose_name='课程描述')
    outline = models.TextField(verbose_name='课程大纲')
    cycle = models.IntegerField(verbose_name='课程周期(月)')
    contents = models.ManyToManyField(CourseContent, verbose_name='课程内容')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '课程信息'
        verbose_name_plural = verbose_name


class ClassType(models.Model):
    '''开班类型(一般是周末，脱产)'''
    name = models.CharField(max_length=64, verbose_name='')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '开班类型'
        verbose_name_plural = verbose_name


class Role(models.Model):
    '''角色信息'''
    name = models.CharField(max_length=32, unique=True, verbose_name='角色名称')
    menus = models.ManyToManyField('Menu', blank=True, verbose_name='权限信息')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '角色'
        verbose_name_plural = verbose_name


class User(AbstractUser):
    '''系统用户'''
    email = models.EmailField(unique=True)
    # 普通外键
    branch = models.ForeignKey('Branch', blank=True, null=True, verbose_name='所属校区')
    roles = models.ManyToManyField('Role', blank=True, verbose_name='用户角色')
    note = models.TextField(blank=True, null=True, default=None, verbose_name='备注')
    date_joined = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return '{}-{}'.format(self.email, self.roles)

    class Meta:
        verbose_name = '系统用户'
        verbose_name_plural = '系统用户'


class PreStudent(models.Model):
    '''意向学员'''
    qq = models.CharField(max_length=64, unique=True, verbose_name='QQ号')
    nick_name = models.CharField(max_length=64, blank=True, null=True, verbose_name='昵称')
    real_name = models.CharField(max_length=32, blank=True, null=True, verbose_name='姓名')
    sex_type = (('male', '男'), ('female', '女'))
    sex = models.CharField(choices=sex_type, default='male', max_length=32, verbose_name='性别')
    birthday = models.DateField(blank=True, null=True, verbose_name='出生日期')
    telephone = models.CharField(max_length=11, blank=True, null=True, verbose_name='手机号')
    email = models.EmailField(blank=True, null=True, verbose_name='邮箱地址')
    source_type = (('qq', 'QQ群'),
                   ('referral', '口碑推荐'),
                   ('website', '官方网站'),
                   ('school', '学校课程'),
                   ('others', '其它'),
                   )
    source = models.CharField(max_length=64, choices=source_type, default='other', verbose_name='学生来源')
    referral_from = models.ForeignKey('self', blank=True, null=True, related_name='推荐人')
    course = models.ManyToManyField('Course', verbose_name='咨询课程')
    class_type = models.ForeignKey('ClassType', verbose_name='开班方式')
    st_note = models.TextField(verbose_name='具体内容')
    work_status_choices = (('student', '学生'), ('employed', '在职'), ('unemployed', '无业'))
    work_status = models.CharField(choices=work_status_choices, max_length=32, default='student', verbose_name='职业状态')
    company = models.CharField(max_length=64, blank=True, null=True, verbose_name='当前公司')
    salary = models.CharField(max_length=64, blank=True, null=True, verbose_name='当前薪资')
    status_choices = (('signed', '已报名'), ('unregistered', '未报名'))
    status = models.CharField(choices=status_choices, max_length=64, default='unregistered',
                              verbose_name='是否报名')
    date = models.DateField(auto_now_add=True, verbose_name='咨询日期')

    def __str__(self):
        return '{}:{}'.format(self.nick_name, self.qq)

    class Meta:
        verbose_name = '意向学员表'
        verbose_name_plural = verbose_name


class Teacher(models.Model):
    '''教师信息'''
    avator = models.ImageField(upload_to='avator/teacher/', default='avator/default.jpg',
                               null=True, blank=True, verbose_name='头像')
    real_name = models.CharField(max_length=32, blank=True, null=True, verbose_name='姓名')
    desc = models.CharField(max_length=512, verbose_name='描述信息')
    user = models.ForeignKey(User, verbose_name='关联用户')

    def __str__(self):
        return self.real_name

    class Meta:
        verbose_name = '教师团队'
        verbose_name_plural = verbose_name


class ClassList(models.Model):
    '''班级信息'''
    name = models.CharField(max_length=32, verbose_name='班级名')
    branch = models.ForeignKey('Branch', verbose_name='校区')
    course = models.ForeignKey('Course', verbose_name='课程')
    class_type = models.ForeignKey('ClassType')
    start_date = models.DateField('开班日期')
    end_date = models.DateField('结业日期', blank=True, null=True)
    teachers = models.ManyToManyField('Teacher', verbose_name='讲师')

    def __str__(self):
        return '{}-{}'.format(self.name, self.course)

    class Meta:
        verbose_name = '班级列表'
        verbose_name_plural = verbose_name
        unique_together = ('course', 'name')


class Student(models.Model):
    '''报名学员'''
    avator = models.ImageField(upload_to='avator/teacher/', default='avator/default.jpg',
                               null=True, blank=True, verbose_name='头像')
    user = models.OneToOneField(User, verbose_name='关联用户', related_name='related_student')
    detail = models.ForeignKey('PreStudent', verbose_name='详细信息')
    my_class = models.ForeignKey('ClassList', verbose_name='所在班级')
    idcard = models.CharField(max_length=18, blank=True, null=True, verbose_name='身份证号', )
    date_end = models.DateField('失效日期')

    def __str__(self):
        return self.detail.real_name

    class Meta:
        verbose_name = '报名学员'
        verbose_name_plural = verbose_name


class CourseRecord(models.Model):
    '''班级上课记录'''
    class_name = models.ForeignKey(ClassList, related_name='related_course_record', verbose_name='班级(课程)')
    course_content = models.ForeignKey('CourseContent', verbose_name='上课内容')
    day_num = models.IntegerField(verbose_name='第N次课')
    date = models.DateField(auto_now_add=True, verbose_name='上课日期')
    teacher = models.ForeignKey('Teacher', default=None, null=True, blank=True, verbose_name='讲师')
    homework_title = models.CharField(max_length=256, blank=True, null=True)
    homework_desc = models.TextField(blank=True, null=True)

    def __str__(self):
        return '<{}>{}-{}'.format(self.class_name, self.course_content, self.day_num)

    class Meta:
        verbose_name = '上课记录'
        verbose_name_plural = '上课记录'
        unique_together = ('class_name', 'course_content')


class ScoreRecord(models.Model):
    '''学习成绩'''
    student = models.ForeignKey('Student', verbose_name='学员')
    course_record = models.ForeignKey(CourseRecord, verbose_name='课程')
    record_choices = (('checked', '正常'),
                      ('late', '迟到'),
                      ('leaved', '缺勤'),
                      ('leave_early', '早退'),
                      )
    record = models.CharField(choices=record_choices, default='checked', max_length=64, verbose_name='上课纪录')
    homework_replay_content = models.TextField('答题详情')
    homework_replay_attach = models.FileField(upload_to='housework/', null=True, default=True, verbose_name='作业附件')
    score_choices = ((100, 'A+'), (90, 'A'),
                     (85, 'B+'), (80, 'B'),
                     (70, 'B-'), (60, 'C+'),
                     (50, 'C'), (40, 'C-'),
                     (-50, 'D'), (0, 'N/A'))
    score = models.IntegerField(choices=score_choices, null=True, blank=True, verbose_name='本次成绩')
    date = models.DateTimeField(auto_now_add=True)
    note = models.CharField(u'备注', max_length=255, blank=True, null=True)

    def __str__(self):
        return '{}:{}-{}'.format(self.student, self.course_record, self.score)

    class Meta:
        verbose_name = '上课成绩纪录'
        verbose_name_plural = verbose_name
        unique_together = ('course_record', 'student')


class JobInfo(models.Model):
    '''就业信息'''
    student = models.OneToOneField('Student', verbose_name='学员ID', unique=True)
    job_photo = models.ImageField(upload_to='job_photo/', default='job_photo/default.jpg',
                                  null=True, blank=True, verbose_name='照片')
    company = models.CharField(max_length=255, blank=True, null=True, verbose_name='公司')
    salary = models.IntegerField(null=True, blank=True, verbose_name='薪水')
    city = models.CharField(max_length=20, null=True, blank=True, verbose_name='城市')
    position = models.CharField(max_length=64, null=True, blank=True, verbose_name='岗位')
    is_recommend = models.BooleanField(default=False, verbose_name='是否推荐')

    def __str__(self):
        return '{}-{}'.format(self.student.detail.real_name, self.company)

    class Meta:
        verbose_name = '就业信息'
        verbose_name_plural = '就业信息'


class CheckingIn(models.Model):
    '''打卡信息'''
    user = models.ForeignKey('User', verbose_name='用户')
    date = models.DateField('考勤日期')
    sign_time = models.TimeField('签到时间')

    def __str__(self):
        return '{}-{}-{}'.format(self.user, self.date, self.sign_time)

    class Meta:
        verbose_name = '考勤记录'
        verbose_name_plural = verbose_name


class CheckingInStatistics(models.Model):
    '''考勤统计'''
    user = models.ForeignKey('User', verbose_name='用户')
    date = models.DateField('考勤日期')
    sign_time = models.TimeField('签到时间', null=True)
    leave_time = models.TimeField('签退时间', null=True)
    status = models.CharField(max_length=30, verbose_name='状态')
    note = models.CharField(max_length=100, null=True, verbose_name='备注')

    def __str__(self):
        return '{}-{}-{}'.format(self.user, self.date, self.status)

    class Meta:
        verbose_name = '考勤统计'
        verbose_name_plural = verbose_name
        unique_together = ('user', 'date')


class Preperty(models.Model):
    '''资产表'''
    type_choices = (('display', '显示器'),
                    ('keyboard', '键盘'),
                    ('mouse', '鼠标'),
                    ('mainframe', '主机'),
                    ('table&chairs', '桌椅'),)
    type = models.CharField(choices=type_choices, max_length=30, verbose_name='资产类型')
    user = models.ForeignKey('User', verbose_name='用户')
    buy_time = models.DateField(verbose_name='购买时间')
    amount = models.IntegerField(verbose_name='单价金额')

    def __str__(self):
        return '{}'.format(type)

    class Meta:
        verbose_name = '资产表'
        verbose_name_plural = verbose_name