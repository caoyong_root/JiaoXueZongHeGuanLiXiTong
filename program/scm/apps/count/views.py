from django.shortcuts import render, HttpResponse
import json
from apps.crm.models import JobInfo
# Create your views here.
def job_info(request):
    job_info = JobInfo.objects.values('student__detail__real_name','company', 'salary','job_photo','city','position')
    info_list = list(job_info)
    for i in info_list:
        i['job_photo'] = '<img src="/media/{}" style="width:100px;height:100px">'.format(i['job_photo'])
    print(info_list)
    server_info_list = {'total': 100, 'rows': info_list}
    return HttpResponse(json.dumps(server_info_list))


def jobinfo(request):
    return render(request, 'job/job_info.html')
