from django.conf.urls import url
from apps.count.views import job_info, jobinfo

urlpatterns = [
    url(r'^job_info/$', job_info, name="job_info"),
    url(r'^jobinfo/$', jobinfo, name='jobinfo')
]
